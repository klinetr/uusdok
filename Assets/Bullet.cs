﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] float _speed = default;
    IEnumerator Start()
    {
        GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * _speed, ForceMode.Impulse);
        yield return new WaitForSeconds(10);
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
