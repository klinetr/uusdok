﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootyGuns : MonoBehaviour
{
    [SerializeField] GameObject _bulletPrefab = default;
    [SerializeField] Transform _bombLocation = default;
    [SerializeField] Transform _gunLocation = default;
    [SerializeField] float _shootTimer = 0.03f;
    [SerializeField] float _bombTimer = 0.3f;
    [SerializeField] LayerMask _hitLayers = default;

    float timer = 0;
    float timer2 = 0;

    void Update()
    {
        timer += Time.deltaTime;
        if(Input.GetMouseButton(1) && timer >= _bombTimer)
        {
            timer = 0;
            Instantiate(_bulletPrefab, _bombLocation.position, _bombLocation.rotation);
        }

        timer2 += Time.deltaTime;
        RaycastHit hitInfo;
        if(Input.GetMouseButton(0) && timer2 >= _shootTimer)
        {
            timer2 = 0;
            if(Physics.Raycast(Camera.main.transform.position,
            Camera.main.transform.forward, out hitInfo, 
            9999f, _hitLayers))
            {
                Debug.DrawLine(_gunLocation.position, hitInfo.point, Color.red, 0.025f );
            }
        }
    }
}
