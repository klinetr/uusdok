﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
using UnityEngine.VFX.Utility;
using Sirenix.OdinInspector;
using System;
using Random = UnityEngine.Random;
using System.Linq;

public sealed class CompositeIntegralTriplet : IEquatable<CompositeIntegralTriplet>
{
    public CompositeIntegralTriplet(int first, int second, int third)
    {
        First = first;
        Second = second;
        Third = third;
    }

    public int First { get; }
    public int Second { get; }
    public int Third { get; }

    public override bool Equals(object other)
    {
        var otherAsTriplet = other as CompositeIntegralTriplet;
        return Equals(otherAsTriplet);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            var hashCode = First;
            hashCode = (hashCode*397) ^ Second;
            hashCode = (hashCode*397) ^ Third;
            return hashCode;
        }
    }

    public bool Equals(CompositeIntegralTriplet other) => 
        other != null && First == other.First && Second == other.Second && Third == other.Third;
}

public class Grid : MonoBehaviour
{
    [SerializeField] int _width;
    [SerializeField] int _depth;
    [SerializeField] int _height;
    [SerializeField] GameObject _gridElementPrefab;

    [SerializeField] int _gridSize;

    [SerializeField] VFXPropertyBinder _binder = default;

    Dictionary<CompositeIntegralTriplet, GridElement> _gridPositions = new Dictionary<CompositeIntegralTriplet, GridElement>();

    void ClearGrid()
    {
        var count = transform.childCount;
        for(int i = count-1; i >= 0; i--)
        {
            DestroyImmediate(transform.GetChild(i).gameObject);
        }
    }

    private void OnEnable()
    {
        GridElement.ElementChanged += OnElementChanged;
    }

    private void OnDisable()
    {
        GridElement.ElementChanged -= OnElementChanged;

    }

    void OnElementChanged()
    {
        if(CheckGrid())
        {
            Debug.Log("you win");
        }
    }

    bool CheckGrid()
    {
        //WORSE ALGORITHM EVER Gimme that O(n^2) shit
        foreach(var element in _gridPositions.Values)
        {
            foreach (var otherElement in _gridPositions.Values)
            {
                if (element.x == otherElement.x && element.y == otherElement.y ||
                element.x == otherElement.x && element.z == otherElement.z ||
                element.y == otherElement.y && element.z == otherElement.z)
                {
                    if (element.Value == otherElement.Value) return false;
                }
            }
        }
        return true;
    }

    void Awake()
    {
        ClearGrid();
        MakeGrid();
        foreach (var element in GetComponentsInChildren<GridElement>())
        {
            try {
                var thing = new CompositeIntegralTriplet(element.x, element.y, element.z);
                _gridPositions.Add(thing, element);
            } catch (System.ArgumentException)
            {
                Debug.Log(element.name);
            }
        }

        var binder = _binder.GetPropertyBinders<MultiplePositionBinder>().FirstOrDefault();

        foreach (var element in _gridPositions.Values)
        {
            binder.Targets.Add(element);
        }
    }

    public Vector3 GetPosition(int x, int y, int z)
    {
        Debug.Log($"{x} {y} {z}");
        return _gridPositions[new CompositeIntegralTriplet(x, y, z)].transform.position;
    }

    public Vector3 GetRandomPosition() =>
        GetPosition(Random.Range(0, _width),Random.Range(0, _height),Random.Range(0, _depth));

    void MakeGrid()
    {
        for(int i = 0; i < _width; i++)
        {
            for(int j = 0; j < _height; j++)
            {
                for(int k = 0; k < _depth; k++)
                {
                    var t = Instantiate(_gridElementPrefab).transform;
                    t.name = $"[({i},{j},{k})]";
                    t.position = new Vector3(
                        (float)i * _gridSize/2, 
                        (float)j * _gridSize/2, 
                        (float)k * _gridSize/2);
                    t.SetParent(transform);
                    var element = t.gameObject.GetComponent<GridElement>();
                    element.x = i;
                    element.y = j;
                    element.z = k;
                }
            }
        }
    }
}
