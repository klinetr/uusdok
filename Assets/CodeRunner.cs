﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodeRunner : MonoBehaviour
{
    
    int _moveRegister = 0;
    int _fireRegister = 0;
    int _faceRegister = 0;
    int _freeRegister1 = 0;
    int _freeRegister2 = 0;

    [SerializeField] string[] _commands;
    [SerializeField] Grid _grid;
    [SerializeField] GameObject _camera;

    bool _enabled = false;

    FlyCam _flyCam;
    CodePanel _codePanel;

    void Awake()
    {
        _flyCam =  FindObjectOfType<FlyCam>();
        _codePanel = FindObjectOfType<CodePanel>();
        _codePanel.gameObject.SetActive(false);
    }

    public void EnableCamera()
    {
        _camera.SetActive(true);
        _codePanel.gameObject.SetActive(true);
        _codePanel.SetText(_commands);
        _codePanel.Select();
        _enabled = true;
    }

    public void DisableCamera()
    {
        _camera.SetActive(false);
        _codePanel.gameObject.SetActive(false);
        _enabled = false;
        _flyCam.SetState(true);
    }


    Vector3 _velocity;

    IEnumerator Start()
    {
        while(true)
        {
            Vector3 doda = _grid.GetRandomPosition();

            while(transform.position != doda)
            {
                transform.position = Vector3.SmoothDamp(transform.position, doda, ref _velocity, 0.3f, 9999f);
                if(Vector3.Distance(doda, transform.position) <= 0.3f)
                {
                    transform.position = doda;
                }
            }

            yield return new WaitForSeconds(3);
        }
    }

    void Update()
    {
        if(_enabled)
        {
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                DisableCamera();
            }
        }
    }
}
