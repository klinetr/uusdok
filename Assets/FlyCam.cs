﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;

public class FlyCam : MonoBehaviour
{
    [SerializeField] float _speed = 10;
    [SerializeField] float _strafeSpeed = 10;

    [SerializeField] LayerMask _hitLayers = default;

    [SerializeField] Image _crosshair = default;
    [SerializeField] CinemachineVirtualCamera vcam = default;
    CinemachinePOV pov = default;



    void Awake()
    {
        pov = vcam.GetCinemachineComponent<CinemachinePOV>();
        Cursor.lockState = CursorLockMode.Locked;
    }

    void OnEnable()
    {
        transform.position = Camera.main.transform.position;
        transform.rotation = Quaternion.LookRotation(Camera.main.transform.forward, Vector3.up);
        pov.m_HorizontalAxis.Value = pov.m_VerticalAxis.Value = 0;
    }

    void Update()
    {
        transform.position = transform.position 
        + Camera.main.transform.forward * Input.GetAxis("Vertical") * Time.deltaTime * _speed
        + Camera.main.transform.right * Input.GetAxis("Horizontal") * Time.deltaTime * _strafeSpeed;

        RaycastHit hitInfo;
        if(Physics.Raycast(Camera.main.transform.position,
            Camera.main.transform.forward, out hitInfo, 
            9999f, _hitLayers))
            {
                _crosshair.color = Color.red;
                
                if(Input.GetMouseButtonDown(3) || Input.GetKeyDown(KeyCode.Space))
                {
                    //Getting dirty
                    hitInfo.collider.SendMessage("EnableCamera");
                    SetState(false);
                }
            }else{
                 _crosshair.color = Color.white;
            }
    }

    public void SetState(bool isActive)
    {
        _crosshair.enabled = isActive;
        gameObject.SetActive(isActive);
    }
}
