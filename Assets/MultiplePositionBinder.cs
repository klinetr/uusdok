﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
using UnityEngine.VFX.Utility;

[AddComponentMenu("VFX/Property Binders/MPB")]
[VFXBinder("Point Cache/MPB")]
public class MultiplePositionBinder : VFXBinderBase
{
    [VFXPropertyBinding("UnityEngine.Texture2D")]
    [SerializeField] ExposedProperty PositionMapProperty = "PositionMap";
    [VFXPropertyBinding("UnityEngine.Texture2D")]
    [SerializeField] ExposedProperty ValueMapProperty = "ValueMap";
    [VFXPropertyBinding("System.Int32")]
    [SerializeField] ExposedProperty PositionCountProperty = "PositionCount";

    public List<GridElement> Targets = null;
    public bool EveryFrame = false;

    private Texture2D positionMap;
    private Texture2D valueMap;
    private int count = 0;


    protected override void OnEnable()
    {
        base.OnEnable();
        UpdateTexture();
    }

    public override bool IsValid(VisualEffect component)
    {
        return Targets != null && component.HasTexture(PositionMapProperty) && component.HasInt(PositionCountProperty) && component.HasTexture(ValueMapProperty);
    }

    public override void UpdateBinding(VisualEffect component)
    {
        if (EveryFrame || Application.isEditor) UpdateTexture();

        component.SetTexture(PositionMapProperty, positionMap);
        component.SetInt(PositionCountProperty, count);
        component.SetTexture(ValueMapProperty, valueMap);

    }

    private void UpdateTexture()
    {
        if (Targets == null || Targets.Count == 0) return;

        count = Targets.Count;

        if(positionMap == null || positionMap.width != count)
        {
            positionMap = new Texture2D(count, 1, TextureFormat.RGBAFloat, false);
        }

        if (valueMap == null || valueMap.width != count)
        {
            valueMap = new Texture2D(count, 1, TextureFormat.RGBAFloat, false);
        }

        List<Color> colors = new List<Color>();
        List<Color> values = new List<Color>();

        foreach(var element in Targets)
        {
            colors.Add(new Color(element.transform.position.x, element.transform.position.y, element.transform.position.z));
            values.Add(new Color(element.Value, 0, 0));
        }

        positionMap.name = gameObject.name + "_PositionMap";
        positionMap.filterMode = FilterMode.Point;
        positionMap.wrapMode = TextureWrapMode.Repeat;
        positionMap.SetPixels(colors.ToArray(), 0);
        positionMap.Apply();

        valueMap.name = gameObject.name + "_ValueMap";
        valueMap.filterMode = FilterMode.Point;
        valueMap.wrapMode = TextureWrapMode.Repeat;
        valueMap.SetPixels(values.ToArray(), 0);
        valueMap.Apply();
    }
}