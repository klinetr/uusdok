﻿using System;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class GridElement : MonoBehaviour
{
    public int x;
    public int y;
    public int z;
    private float timer2;
    [SerializeField] float _shootTimer = 0.5f;
    [SerializeField] private LayerMask _hitLayers;
    Collider _collider;

    private int value = 0;

    public int Value => value;

    public static event Action ElementChanged;

    private void Awake()
    {
        _collider = GetComponent<Collider>();
    }

    private void OnEnable()
    {
        value = UnityEngine.Random.Range(0, 4);
    }

    private void Update()
    {
        timer2 += Time.deltaTime;
        RaycastHit hitInfo;
        if (Input.GetMouseButton(0) && timer2 >= _shootTimer)
        {
            timer2 = 0;
            if (Physics.Raycast(Camera.main.transform.position,
            Camera.main.transform.forward, out hitInfo,
            9999f, _hitLayers))
            {
                if (hitInfo.collider == _collider)
                {
                    value++;
                    if (value > 3) value = 0;
                    ElementChanged?.Invoke();
                }
            }
        }
    }

}
