﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class CodePanel : MonoBehaviour
{
    [SerializeField] TMP_InputField _inputField = default; 

    public void SetText(params string[] lines)
    {
        _inputField.text = string.Join("\n", lines);
    }

    public void Select()
    {
        _inputField.Select();
        _inputField.MoveTextEnd(false);
    }
}
